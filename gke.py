import pulumi
from pulumi import export, Output
from pulumi_gcp.config import project, zone
from pulumi_gcp.container import Cluster, ClusterNodeConfigArgs

NODE_COUNT = 1
NODE_MACHINE_TYPE = 'e2-medium'

# Create a cluster with 1 worker node.
k8s_cluster = Cluster('gke-cluster',
    initial_node_count=NODE_COUNT,
    node_config=ClusterNodeConfigArgs(
        machine_type=NODE_MACHINE_TYPE
    )
)

export('gke_cluster_name', k8s_cluster.name)

#
## We won't use the following Kubeconfig in this tutorial.
## The code is just for illustration if you want to deploy K8s resources by Pulumi.
#
k8s_info = Output.all(k8s_cluster.name, k8s_cluster.endpoint, k8s_cluster.master_auth)

k8s_config = k8s_info.apply(
    lambda info: """apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: {0}
    server: https://{1}
  name: {2}
contexts:
- context:
    cluster: {2}
    user: {2}
  name: {2}
current-context: {2}
kind: Config
preferences: {{}}
users:
- name: {2}
  user:
    auth-provider:
      config:
        cmd-args: config config-helper --format=json
        cmd-path: gcloud
        expiry-key: '{{.credential.token_expiry}}'
        token-key: '{{.credential.access_token}}'
      name: gcp
""".format(info[2]['cluster_ca_certificate'], info[1], '{0}_{1}_{2}'.format(project, zone, info[0])))

export('kubeconfig', k8s_config)

