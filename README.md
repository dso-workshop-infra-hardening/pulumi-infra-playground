# pulumi-infra-playground

This project is used to create the GCP infrastructure resources required for the workshop.

# 前置作業
## GCP
### 建立專案
1. 至 GCP Console 建立 GCP 專案，此專案以 `dso-workshop-1113` 命名

### 啟用 API
- Kubernetes Engine API
- Compute Engine API

### 建立服務帳戶
由於後續將透過自動化部署基礎設施資源，為了讓 GitLab 有存取權限，因此需要一個服務帳戶(Service Account)來存取 GCP 資源。
> [說明文件](https://cloud.google.com/docs/authentication/getting-started)
1. 選擇 Project
2. 選擇左上角選單中的 **IAM & Admin**，再點選左側選單的 **Service Accounts**，並選擇 **CREATE SERVICE ACCOUNT**
3. 在 **Service account details** 填入以下資訊
   > 由於 Build 階段與 Deploy 階段所需採取的動作不同，因此分離此兩個階段的服務帳戶權限：
   - Build 階段使用：
      - `Name`: pulumi-viewer
      - `Description`: This service account is used by GitLab CI/CD for pulumi preview.
   - Deploy 階段使用：
      - `Name`: pulumi-provisioner
      - `Description`: This service account is used by GitLab CI/CD for pulumi deploy.
4. 在 **Grant this service account access to the project** 授予以下角色:
    > a. Roles 為依據後續需要部署的基礎設施資源 (GKE, Instance, Firewall...) 配置，請遵循最小權限原則。

    > b. 此處亦可先不配置，若 Service Account 已建立，請至 **IAM & Admin** -> **IAM** -> **ADD** -> 於 **New Principals** 搜尋剛建立的服務帳戶，並授予以下角色。

    > c. 由於我們已根據先前經驗配置好權限，若您基於此專案建立您的 CI/CD 但遇上失敗，請查看 CI Job 的輸出日誌，確認錯誤訊息是否與角色權限不足或 API 未啟用有關。您可從[此處](https://cloud.google.com/iam/docs/understanding-roles)取得您所需的角色。

   | 服務帳戶        | Role                                                                                                                                                        |
   | --------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
   | pulumi-viewer   | `Viewer`                                                                                                                                                    |
   | pulumi-deployer | `Compute Instance Admin (v1)` <br> `Compute Network Admin` <br> `Compute Security Admin` <br> `Kubernetes Engine Cluster Admin` <br> `Service Account User` |
5. 回到 **Service Accounts** 選單中，並點選剛所建立的服務帳戶
6. 選擇 **KEYS** > **ADD KEY** > **Create new key** > **Key type** 選擇 *JSON*
6. 點選 **CREATE** 後下載金鑰到本機端
   > 此為機密資訊，下載後請妥善保管。

## Pulumi
1. 根據[說明文件](https://www.pulumi.com/docs/intro/console/accounts/#access-tokens)建立 Access Token
2. 至 Pulumi Console 建立一個專案
   - 在 Stack 的欄位填入 `dev`

## GitLab CI/CD
依據[說明文件](https://www.pulumi.com/registry/packages/gcp/installation-configuration/) 設定以下的 GitLab CI/CD 變數 (請至GitLab 專案或群組中 **Settings** > **CI/CD** > **Variables** 新增變數):

|                     | Key                                          | Value                      | Type     | Protected |  Masked  |
| ------------------- | -------------------------------------------- | -------------------------- | -------- | :-------: | :------: |
| project             | GCLOUD_PROJECT                               | dso-workshop-1113          | Variable |     N     |    N     |
| region              | GCLOUD_REGION                                | asia-east1                 | Variable |     N     |    N     |
| zone                | GCLOUD_ZONE                                  | asia-east1-a               | Variable |     N     |    N     |
| credentials         | (GOOGLE_CREDENTIALS) <br> 請改為下面的鍵值： | -                          | -        |     -     |    -     |
|                     | GCP_VIEWER                                   | `pulumi-viewer` 金鑰內容   | File     |     N     | 無法啟用 |
|                     | GCP_DEPLOYER                                 | `pulumi-deployer` 金鑰內容 | File     |     Y     | 無法啟用 |
| PULUMI_ACCESS_TOKEN | PULUMI_ACCESS_TOKEN                          | Pulumi Access Token        | Variable |     N     |    Y     |


# branch 說明
- `main`: 實際部署
- `cleanup`: 刪除所有部署資源 (於活動結束後執行)
- 其他分支: 僅會 preview 變更的資源

# 基礎設施資源說明
此專案將建立以下的資源：
1. GKE
2. Shared VM
   - 已安裝 docker, kubectl, helm, minikube 環境
   - 若要新增使用者帳戶，請至 `ansible-playbooks` 資料夾執行腳本
