import pulumi
from pulumi import ResourceOptions
from pulumi_gcp import compute

VM_COUNT = 5  # number of shared vm

# Read init_script.txt
with open('scripts/init_script.txt', 'r') as init_script:
    data = init_script.read()
script = data

# Create VPC network
compute_network = compute.Network("default-network")

# Configure firewall to allow SSH (port 22)
compute_firewall = compute.Firewall(
    "firewall",
    network = compute_network.name,
    allows = [compute.FirewallAllowArgs(
        protocol = "tcp",
        ports = ["22"]
    )]
)

instance_info = []

for i in range(1, VM_COUNT + 1):
    # Create IP address
    instance_addr = compute.Address(
        f"shared-vm-address-{i}",
        region = "asia-east1"
    )

    # Create VM
    compute_instance = compute.Instance(
        f"shared-vm-{i}",
        zone = "asia-east1-a",
        boot_disk = compute.InstanceBootDiskArgs(
            initialize_params = compute.InstanceBootDiskInitializeParamsArgs(
                image = "ubuntu-1804-bionic-v20210211"
            )
        ),
        machine_type = "e2-medium",
        network_interfaces = [compute.InstanceNetworkInterfaceArgs(
            network = compute_network.name,
            access_configs = [compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip = instance_addr.address
            )],
        )],
        opts = ResourceOptions(depends_on = [compute_firewall]),
        metadata_startup_script = script,  # Set startup_script
        metadata = {
            # the format is <username>:<id_rsa.pub>
            "ssh-keys": "player456:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCo0Pd9z6ERsOh2fYXAeWkpPe9XDhxSVVGUvP5LhaEMPweKo2tb5qKLEQkS7SydIbuaL75govVvCTD3RktvJWPylZCMeCKfHCen9qEXONUSwD8o3dJfrp+X7JoDbOKCPv+30aPESvis2AFhBiXZ8WMSIIUV4P4wyCavBdbU+xSdIdXuHsunnQeH0ikLvw+9LKm3QwDhg4sm5rz/L8FbAv5vU9YkgjhDcX86u6ygpcW31yCvdiyWQKZmouKU8Ntfrrvyism1iMIkBMSDxkMQuMIEQF/VjlgWHhiemHoidkTIlaV3q7lWW9Ktz/iLlhRThfci3Lu41jIbFVIpnBUIHJCLttlEa26ywiW/peVRix7F8LQI9YXGQygeb87+5Vctk+E2vjyD/qZslEEsmELJIGkw7ifaA7LguskfiXNQD8UBX/lZ9CilwiS1mrcUGpAXUmT0FvUTYFxxxSdQEU0MQxjB9vl7HnKxLYMsOHDLDcGaTJ4nauYqKzWlFO5E8JqwQhs= player456@shared-vm"
        },
        tags = ["workshop", "dev"]
    )
    instance_info.append({
        "name": compute_instance.name,
        "address": instance_addr.address
    })

pulumi.export("instance_info", instance_info)
