> `playground.key` 已透過 git-crypt 加密

1. 透過加密金鑰解密 `playground.key`
    ```sh
    $ git-crypt unlock path/to/git-crypt-key
    ```
2. 將 `inventory` 中的 `<shared-vm-ip>` 變更為 shared-vm 的 IP
3. 執行 Ansible playbook
    ```sh
    $ ansible-playbook -i inventory add-new-user.yml
    ```